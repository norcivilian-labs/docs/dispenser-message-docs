# Requirements

# message
## ugly nephew guitar
 - user must import vk backup for message records 
## frog orphan setup
 - user must import wa backup for message records 
## beach chunk upset
 - user must import email backup for message records 
## output item pitch
 - user must import ig backup for message records 
## text village echo
 - user must import tg backup for message records 
## dress small resource
 - user must import ap backup for message records 
## easy page solution
 - user must import x backup for message records 
## electric solve pave
 - user must import matrix backup for message records 
## offer person super
 - user must update dataset branch metadata for pedigree

# basic csvs requirements
## little patient night
- user must import a dataset with git clone
## talent rabbit region
- user must export a dataset with git push
## elegant divert glad
- user must authenticate with git HTTP smart service
## cream piano tooth
- user must authenticate with git HTTP dumb service
## sort multiply rice
- user must authenticate with gitlab over OAuth
## exercise fragile cage
- user must authenticate with github over OAuth
## riot side foil
- user must authenticate with gitea over OAuth
## limb renew scale
- desktop user must open dataset locally
## ribbon second oak
- desktop user must open multiple windows
## detect victory grow
- browser user must see a warning that clearing cache will destroy data 
## brisk turkey number
- user must download zip archive of the dataset
