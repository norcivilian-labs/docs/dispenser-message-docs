# dispenser-message-docs

Documentation for [message dispenser](https://gitlab.com/norcivilian-labs/dispenser-message), built with [mdBook](https://github.com/rust-lang/mdBook) and hosted at [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
